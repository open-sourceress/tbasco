"""
The request module exports the `Request` ABC, for requests to the TBA API.
"""

__all__ = ['Request']

from abc import ABC, abstractmethod
from typing import Generic, TypeVar, Any

from . import client, model

R = TypeVar('R', bound=model.Model)  # Response


class Request(ABC, Generic[R]):
    """
    A parent class for requests to the TBA API.

    Instances of this class can be `await`ed to execute the request and return the response. To execute multiple requests
    concurrently, pass the requests to `TBA.request` instead.

    Subclasses must override the `route` property to provide the route to be fetched.
    """

    def __init__(self, client: 'client.TBA') -> None:
        self._client = client

    @property
    @abstractmethod
    def route(self) -> str:
        """
        The route to request.

        This is relative to the client's base_url and must include the leading slash.
        """

    @abstractmethod
    def parse_response(self, body: Any) -> R:
        """
        Parse the body of a response, before it is returned.

        This exists to e.g. transform a JSON dictionary into a `Model` object.
        :param body: the response body, as a JSON value
        :return: the parsed model.
        """

    def __repr__(self) -> str:
        ty = '{0.__module__}.{0.__qualname__}'.format(type(self))
        return f'<{ty} route={self.route!r}>'

    async def __request(self) -> R:
        (response,) = await self._client.request(self)
        return response

    def __await__(self):
        return self.__request().__await__()
