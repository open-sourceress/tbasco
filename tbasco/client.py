"""
The client module defines the `TBA` class, a client for interacting with the TBA API.
"""

__all__ = ['TBA']

import asyncio
from typing import Tuple, Union

import aiohttp

from .request import Request
from .status import StatusRequest
from .team import TeamRequest, TeamsRequest


class TBA:
    """
    A client for the Blue Alliance API.

    The TBA class allows accessing the TBA HTTP API version 3.

    Methods of this class allow requesting data from the TBA API. All methods that request data from the API return
    `Request`s, which must be `await`ed or passed to the `request` method in order to actually make the request. All
    HTTP requests are made asynchronously.
    """

    def __init__(self, api_key: str, *, http_session: aiohttp.ClientSession = None,
                 base_url: str = 'https://thebluealliance.com/api/v3') -> None:
        """
        Create a TBA client.

        API keys can be created at https://thebluealliance.com/account.

        :param api_key: a TBA Read API key to authenticate requests with.
        :param http_session: a session to use for HTTP requests. If not provided, a new session will be created.
        :param base_url: The base url for API requests, without the trailing slash.
        """
        self._headers = {'X-TBA-Auth-Key': api_key}
        if http_session is None:
            self._session = aiohttp.ClientSession()
            self._owns_session = True
        else:
            self._session = http_session
            self._owns_session = False
        self._base_url = base_url

    async def close(self) -> None:
        """
        Close this client and release the underlying resources.

        Note: if a session was passed to the initializer, the session is not closed and it is the caller's
        responsibility to ensure the session is properly closed.
        """
        if self._owns_session:
            await self._session.close()
        self._session = None

    @property
    def closed(self) -> bool:
        """Has this client been closed?"""
        return self._session is None or self._session.closed

    async def __aenter__(self) -> 'TBA':
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb) -> bool:
        await self.close()
        return False

    async def request(self, *requests: Request) -> Tuple:
        """
        Make multiple requests in parallel.

        This method launches all requests concurrently and returns when they all complete. The return value is a tuple
        containing the response for each request in the order they were provided.
        """
        return tuple(await asyncio.gather(*[self._make_request(req) for req in requests]))

    async def _make_request(self, req: Request):
        async with self._session.get(self._base_url + req.route, headers=self._headers) as resp:
            resp.raise_for_status()
            return req.parse_response(await resp.json())

    def status(self) -> StatusRequest:
        """Request the status of the TBA API."""
        return StatusRequest(self)

    @staticmethod
    def _validate_team_key(team_key: Union[int, str]) -> str:
        if isinstance(team_key, int):
            if team_key > 0:
                return f'frc{team_key}'
            else:
                raise ValueError(f'cannot request team number {team_key}: team numbers must be greater than 0')
        elif isinstance(team_key, str):
            if team_key.startswith('frc') and team_key[3:].isnumeric():
                return team_key
            else:
                raise ValueError(f'cannot request team key {team_key!r}: team keys must be "frc" followed by a team number')
        else:
            type_name = '{0.__module__}.{0.__qualname__}'.format(type(team_key))
            raise TypeError(f'cannot request team key {team_key!r} ({type_name}): team numbers must be ints or strings')

    def team(self, team_key: Union[int, str]) -> TeamRequest:
        """Request a team by number or team key."""
        return TeamRequest(self, self._validate_team_key(team_key))

    def teams(self, page: int, year: int = None) -> TeamsRequest:
        """Request up to 500 teams by page number."""
        return TeamsRequest(self, page, year)
