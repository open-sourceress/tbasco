"""
tbasco
===

A wrapper for The Blue Alliance API, with spice.
"""

__author__ = 'Cynthia W'
__version__ = '0.0.0'
__all__ = ['TBA']

from .client import TBA
