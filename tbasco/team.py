__all__ = ['Team', 'TeamRequest', 'TeamsRequest']

from typing import Any, Optional, List, cast

from . import client
from .model import Model, Property, SimpleProperty
from .request import Request


class TeamRequest(Request['Team']):
    """
    A request to the `/team/{team_key}` endpoint, to fetch a team by team key.

    Instances of this class should be created using the `TBA.team` method.
    """

    def __init__(self, client: 'client.TBA', team_key: str) -> None:
        super().__init__(client)
        self.team_key = team_key

    @property
    def route(self) -> str:
        return f'/team/{self.team_key}'

    def parse_response(self, body: Any) -> 'Team':
        return Team(self._client, body)

    def years_participated(self) -> 'TeamYearsRequest':
        """Get a list of years in which the team participated in at least one competition."""
        return TeamYearsRequest(self._client, self.team_key)


class TeamsRequest(Request[List['Team']]):
    """
    A request to the `/teams/{page_num}` or `/teams/{year}/{page_num}` endpoint, to fetch up to 500 teams by page number.

    Instances of this class should be created using the `TBA.teams` method.
    """

    def __init__(self, client: 'client.TBA', page_num: int, year: Optional[int]) -> None:
        super().__init__(client)
        self.page_num = page_num
        self.year = year

    @property
    def route(self) -> str:
        if self.year is None:
            return f'/teams/{self.page_num}'
        else:
            return f'/teams/{self.year}/{self.page_num}'

    def parse_response(self, body: Any) -> List['Team']:
        return [Team(self._client, entry) for entry in body]


class TeamYearsRequest(Request[List[int]]):
    """
    A request to the `/team/{team_key}/years_participated` endpoint, to fetch the years a team is/was active.

    Instances of this class should be created using the `TeamRequest.years_participated` or `Team.years_participated` methods.
    """

    def __init__(self, client: 'client.TBA', team_key: str) -> None:
        super().__init__(client)
        self.team_key = team_key

    @property
    def route(self) -> str:
        return f'/team/{self.team_key}/years_participated'

    def parse_response(self, body: Any) -> List[int]:
        return cast(List[int], body)


class Team(Model):
    """
    An FRC team.

    This model is created by `TeamRequest`s, and corresponds to the `Team` model in TBA's API schema.
    """

    key: Property[str] = SimpleProperty(doc='TBA team key with the format frcXXXX with XXXX representing the team number.')
    number: Property[int] = SimpleProperty('team_number', doc='Official team number issued by FIRST.')
    name: Property[str] = SimpleProperty(doc='Official long name registered with FIRST.')
    nickname: Property[str] = SimpleProperty(doc='Team nickname provided by FIRST.')
    rookie_year: Property[Optional[int]] = SimpleProperty(doc='First year the team officially competed.')
    motto: Property[Optional[str]] = SimpleProperty(doc="Team's motto as provided by FIRST. This field is deprecated and will return None.")
    website: Property[Optional[str]] = SimpleProperty(doc='Official website associated with the team.')

    city: Property[Optional[str]] = SimpleProperty(doc='City of team derived from parsing the address registered with FIRST.')
    state_prov: Property[Optional[str]] = SimpleProperty(doc='State of team derived from parsing the address registered with FIRST.')
    country: Property[Optional[str]] = SimpleProperty(doc='Country of team derived from parsing the address registered with FIRST.')
    postal_code: Property[Optional[str]] = SimpleProperty(doc='Postal code from the team address.')
    address: Property[Optional[str]] = SimpleProperty(doc='Will be None, for future development.')
    latitude: Property[Optional[float]] = SimpleProperty('lat', doc='Will be None, for future development.')
    longitude: Property[Optional[float]] = SimpleProperty('lng', doc='Will be None, for future development.')
    gmaps_place_id: Property[Optional[str]] = SimpleProperty(doc='Will be None, for future development.')
    gmaps_url: Property[Optional[str]] = SimpleProperty(doc='Will be None, for future development.')

    def years_participated(self) -> 'TeamYearsRequest':
        """Get a list of years in which the team participated in at least one competition."""
        return TeamYearsRequest(self._client, self.key)
