"""
The model module exports the `Model` ABC, for models for responses from the TBA API.
"""

__all__ = ['Model', 'Property', 'SimpleProperty']

from abc import ABC, abstractmethod
from typing import Any, cast, Generic, Iterator, Optional, Type, TYPE_CHECKING, TypeVar

from . import client

if TYPE_CHECKING:
    from typing import Mapping

    JSON_Object = Mapping[str, Any]
else:
    from collections.abc import Mapping as JSON_Object


class Model(JSON_Object):
    """
    A parent class for responses returned by the TBA API.

    `Model` instances allow retrieving data via both attribute syntax and dict-like subscripting syntax. Attribute
    lookup returns objects that are more ergonomic to use (e.g. `datetime`s parsed from timestamp strings),
    while subscripting returns the JSON values directly. Therefore, attributes that are provided by the TBA API but not
    exposed through attribute-lookup are still available through subscripting.
    """

    def __init__(self, client: 'client.TBA', data: JSON_Object) -> None:
        self._client = client
        self._data = data

    def __getitem__(self, key: str) -> Any:
        return self._data[key]

    def __len__(self) -> int:
        return len(self._data)

    def __iter__(self) -> Iterator[str]:
        return iter(self._data)

    def __repr__(self) -> str:
        ty = '{0.__module__}.{0.__qualname__}'.format(type(self))
        return f'<{ty} {self._data!r}>'


T = TypeVar('T')
M = TypeVar('M', bound=Model)


class Property(ABC, Generic[T]):
    """
    A parent class for descriptors to be used on `Model` instances.
    """
    # Because __xxx attributes set normally are mangled, this won't conflict with subclasses unless they do weird things
    _CACHED_PREFIX = '__propertycache_'

    def __init__(self, item_name: str = None, *, doc: str = None) -> None:
        """
        Create a descriptor for an attribute on a `Model` class.

        If the attribute name is the same as the key to lookup in the raw TBA response, `item_name` can be omitted
        and will be inferred.
        :param item_name: the name to look up, if different from this descriptor's name
        :param doc: the docstring for this property, if any
        """
        self.__attr_name: Optional[str] = None
        self.__cached_name: Optional[str] = None
        self._item_name: Optional[str] = item_name
        self.__doc__ = doc  # assign even if it's None; prevents inheriting the class-level docs in help(SomeModelClass)

    def __set_name__(self, owner: Type[M], name: str) -> None:
        self.__attr_name = name
        self.__cached_name = self._CACHED_PREFIX + name
        if self._item_name is None:
            self._item_name = name

    @abstractmethod
    def parse_object(self, raw_value: Any) -> T:
        """Convert a JSON object from a TBA response into a proper object."""

    def __get__(self, instance: Optional[M], owner: Type[M]) -> T:
        if instance is None:
            return self
        if self._item_name is None or self.__attr_name is None or self.__cached_name is None:
            # This most likely means the descriptor was created outside of a class definition
            raise RuntimeError('Property name was not provided and cannot be inferred')

        if hasattr(instance, self.__cached_name):
            return getattr(instance, self.__cached_name)

        value = self.parse_object(instance[self._item_name])
        setattr(instance, self.__cached_name, value)
        return value


class SimpleProperty(Property, Generic[T]):
    """
    A simple property, which passes the raw value through unchanged.

    This property is useful for bools, ints, strings, and other values that need no parsing.
    """

    def parse_object(self, raw_value: Any) -> T:
        return cast(T, raw_value)
