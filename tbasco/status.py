__all__ = ['Status', 'StatusRequest']
from typing import List

from .model import Model, Property, SimpleProperty
from .request import Request


class StatusRequest(Request['Status']):
    """
    A request to the `/status` endpoint, to get the status of the API.

    Instances of this class should be created using the `TBA.status` method.
    """

    @property
    def route(self) -> str:
        return '/status'

    def parse_response(self, body) -> 'Status':
        return Status(self._client, body)


class Status(Model):
    """
    The status of the Blue Alliance API.

    This model is created by `StatusRequest`s, and corresponds to the `API_Status` model in TBA's API schema.
    """

    current_season: Property[int] = SimpleProperty(doc='Year of the current FRC season.')
    max_season: Property[int] = SimpleProperty(doc='Maximum FRC season year for valid queries.')
    is_datafeed_down: Property[bool] = SimpleProperty(doc='True if the entire FMS API provided by FIRST is down.')
    down_events: Property[List[str]] = SimpleProperty(doc='An array of strings containing event keys of any active events that are no longer updating.')
